import React, { useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';

import Header from './components/Header';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen';
import GameOver from './screens/GameOver';

const fetchFonts = () => {
  Font.loadAsync({
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
    'open-sans-regular': require('./assets/fonts/OpenSans-Regular.ttf')
  });
}

export default function App() {

  const [userNumber, setUserNumber] = useState();
  const [guessRounds, SetGuessRounds] = useState(0);
  const [dataLoaded, setDataLoaded] = useState(false);

  if(!dataLoaded){
    return <AppLoading
             startAsync={fetchFonts}
             onFinish={()=>setDataLoaded(true)}
             onError={(err)=>consolelog(err)}
            />
  }

  const callBackGameStarted = (selectedNumber) => {
    setUserNumber(selectedNumber);
    SetGuessRounds(0);
  }

  const gameOverHandler = (numberOfRounds) => {
    SetGuessRounds(numberOfRounds);
  }

  const newGameHandler = () => {
    console.log("here i am")
    setUserNumber(null);
    SetGuessRounds(0);
  }

  return (
    <View style={styles.screenView}>
      <Header title="Guess the Number" />
      {!userNumber &&
      <StartGameScreen callBackGameStarted={callBackGameStarted}/>
      }
      {userNumber && guessRounds <= 0 &&
      <GameScreen
       callBackGameStarted={callBackGameStarted}
      confirmedNumber={userNumber}
      onGameOver={gameOverHandler}/>
      }
      {guessRounds > 0 &&
      <GameOver 
        roundsNumber={guessRounds}
        userNumber={userNumber}
        newGameHandler={newGameHandler}
      />
      }
    </View>
  );
}

const styles = StyleSheet.create({
  screenView: {
    flex: 1
  }
});
