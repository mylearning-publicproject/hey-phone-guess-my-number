import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const  NumberContainer = (props) => {
    return(
        <View style={styles.container}>
            <Text>
                {props.children}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        borderWidth: 2,
        color: 'black',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        marginTop: 10
    }
})



export default NumberContainer;