import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = (props) => {
    return (
        <View style={{...styles.card, ...props.styles}}>
            {props.children}
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        alignItems: 'center',
        shadowColor: 'black',
        borderColor: 'black',
        shadowOffset: {width:0, height:2},
        shadowRadius: 6,
        shadowOpacity: 0.26,
        backgroundColor: 'white',
        padding:20,
        elevation: 5,
        borderRadius: 10
    }
})




export default Card;