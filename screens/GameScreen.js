import React, { useState, useRef, useEffect } from 'react';
import { View, Text, StyleSheet, Button, Alert } from 'react-native';
import NumberContainer from '../components/NumberContainer';
import Card from '../components/Card';

const generateRandomBetween = (min, max, exclude) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    const rndNum = Math.floor(Math.random() * (max-min)) + min;
    if(rndNum === exclude) {
        return generateRandomBetween(min, max, exclude);
    } else {
        return rndNum;
    }
}

const GameSection = (props) => {

    const [currentGuess, setCurrentGuess] = useState(generateRandomBetween(1, 100, props.confirmedNumber));

    const [rounds, setRounds] = useState(0);

    const currentLow = useRef(1);
    const currentHigh = useRef(100);

    useEffect(()=>{
        if(currentGuess === props.confirmedNumber){
            props.onGameOver(rounds)
        }
    },[currentGuess, props.onGameOver, props.confirmedNumber])

    const nextGuessHandler = direction => {
        if((direction === 'lower' && currentGuess < props.confirmedNumber) ||
        (direction=== 'higher' && currentGuess > props.confirmedNumber)){
            Alert.alert('Dont Lie!', ' You know that this is wrong...', [{text: 'Sorry', style:'cancel'}])
            return;
        };
        if(direction === 'lower'){
            currentHigh.current = currentGuess;
        } else {
            currentLow.current = currentGuess;
        }
        setCurrentGuess(generateRandomBetween(currentLow.current, currentHigh.current, currentGuess));
        setRounds(rounds => rounds + 1)
    }

    return(
        <View style={styles.screen}>
            <Text>Opponent's Guess </Text>
                <NumberContainer>
                    {currentGuess}
                </NumberContainer>
            <Card styles={styles.buttonContainer}>
                <Button
                    title="LOWER"
                    onPress={nextGuessHandler.bind(this, 'lower')}
                />
                <Button 
                    title="HIGHER"
                    onPress={nextGuessHandler.bind(this, 'higher')}
                />
            </Card>
            <Button 
             title="Exit Game"
             color="red"
             onPress={()=>props.callBackGameStarted()}
             />
        </View>
    );

}

const styles = StyleSheet.create({
    screen:{
        flex: 1,
        padding: 10,
        alignItems: "center"
    },
    buttonContainer:{
        flexDirection: 'row',
        justifyContent:"space-evenly",
        width: 300,
        maxWidth: '80%',
        margin: 20
    }
})






export default GameSection;