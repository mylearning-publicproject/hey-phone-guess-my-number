import React from 'react';
import { View, Text, StyleSheet, Image, Button } from 'react-native';

const GameOver = (props) => {
    return(
        <View>
        {console.log("Game is over RENDER")}
            <Text styles={styles.screen}>
                The Game is Over!
            </Text>
            <Text>
                Number of rounds: {props.roundsNumber}
            </Text>
            <Text>
                User Number: {props.userNumber}
            </Text>
            <Button onPress={props.newGameHandler} title="New Game"/>
        </View>
    )
}

const styles= StyleSheet.create({
    screen:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        color:'black'
    }
})

export default GameOver;