import React, { useState } from "react";
import { View, Text, StyleSheet, Button, TouchableWithoutFeedback, Keyboard, Alert } from "react-native";
import Card from "../components/Card";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";

const StartGameScreen = (props) => {
  const [enteredValue, setEnteredValue] = useState("");
  const [confirmed, setConfirmed] = useState(false);
  const [selectedNumber, setSelectedNumber] = useState('Please enter');
  const [showSelectedSection, setShowSelectedSection] = useState(false)

  const numberInputHandler = (inputText) => {
    // setEnteredValue(inputText.replace(/[^1-9]/g, ""));
    setEnteredValue(inputText);
  };

  const confirmedInputHandler = () => {
    const chosenNumber = parseInt(enteredValue);
    if(isNaN(chosenNumber) || chosenNumber <= 0) {
      Alert.alert('InvalidNumber', 'Number has to be a number between 1 and 99',
       [{text: 'Okay', style: 'destructive', onPress: ()=>{setEnteredValue(''), setConfirmed(false)}}])
        return;
    }
    setConfirmed(true);
    setSelectedNumber(chosenNumber);
    setEnteredValue('');
    Keyboard.dismiss();
    setShowSelectedSection(true);
  }

  
  return (
    <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss()}}>
        <View style={styles.screen}>
        {!showSelectedSection &&
        <View>
        <Text style={styles.title}> Start a New Game!</Text>
        <Card styles={styles.inputContainer}>
            <Text> Select a Number </Text>
            <Input
            style={styles.input}
            blurOnSubmit
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="numeric"
            maxLength={2}
            onChangeText={numberInputHandler}
            value={enteredValue}
            />
            <View style={styles.buttonContainer}>
            <View style={styles.button}>
                <Button title="Reset" color="black" onPress={() => {setConfirmed(true), setEnteredValue('')}} />
            </View>
            <View style={styles.button}>
                <Button title="Confirm" onPress={confirmedInputHandler} />
            </View>
            </View>
        </Card>
        </View>
        }
        {showSelectedSection &&
            <Card styles={styles.ConfirmStyles}>
                <Text>
                   You selected:
                </Text>
                <NumberContainer>
                  {selectedNumber}
                </NumberContainer>
                <Button
                 title="Start Game"
                 fontSize="bold"
                 onPress={()=>props.callBackGameStarted(selectedNumber)}
                 />
                <View>
                  <Button 
                  title="Enter a different Number"
                  color="grey"
                  onPress={()=>setShowSelectedSection(false)}
                  />
                </View>
                
            </Card>
        }
        </View>
    </TouchableWithoutFeedback>
  );
};

export default StartGameScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    marginVertical: 10,
    color: 'orange',
    paddingLeft: 50,
    paddingBottom: 20
  },
  input: {
    width: 50,
    textAlign: "center",
  },
  inputContainer: {
    width: 300,
    maxWidth: "80%",
    flexDirection: "column",
    // justifyContent: 'center',
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    paddingHorizontal: 15,
  },
  button: {
    width: 120,
  },
  ConfirmStyles:{
    margin: 20,
  }
});
